package com.greenhouse.interview;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import com.google.common.base.Strings;
import com.greenhouse.interview.api.Actor;
import com.greenhouse.interview.api.SocialType;
import com.greenhouse.interview.exercise.Actor1;
import com.greenhouse.interview.exercise.Actor2;
import com.greenhouse.interview.exercise.Actor3;
import com.greenhouse.interview.exercise.Actor4;
import com.greenhouse.interview.exercise.Actor5;
import com.greenhouse.interview.exercise.Actor6;
import com.greenhouse.interview.exercise.Actor7;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.BeforeScenario;
import com.tngtech.jgiven.annotation.Quoted;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class SocialsStage extends Stage<SocialsStage> {

  private final String name = "actorName";

  private SocialsFacade instance;
  private Actor testActor;

  @BeforeScenario
  public void setUp() {
    instance = new SocialsFacade(TestData.INSTANCE.socials);
  }

  public SocialsStage an_actor_listening_for_all_events() {
    testActor = new Actor(name, (s) -> true);
    return self();
  }

  public void an_actor_listening_for_events_from_user_efteling() {
    testActor = new Actor1(name);
  }

  public SocialsStage an_actor_listening_for_events_containing_disney() {
    testActor = new Actor2(name);
    return self();
  }

  public SocialsStage an_actor_listening_for_events_not_containing_disney() {
    testActor = new Actor3(name);
    return self();
  }

  public SocialsStage an_actor_listening_for_events_of_type_video() {
    testActor = new Actor4(name);
    return self();
  }

  public SocialsStage an_actor_listening_for_events_before_given_date() {
    testActor = new Actor5(name);
    return self();
  }

  public SocialsStage an_actor_listening_for_events_after_given_date() {
    testActor = new Actor6(name);
    return self();
  }

  public SocialsStage an_actor_listening_for_events_of_type_facebook() {
    testActor = new Actor7(name);
    return self();
  }

  public SocialsStage the_events_are_sourced() {
    CountDownLatch latch = new CountDownLatch(1);
    instance.requestAccess(testActor, latch);
    try {
      latch.await(100, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) { }
    return self();
  }

  public SocialsStage the_actor_received_a_total_of_$_events(int count) {
    assertThat(testActor.eventCount(), is(equalTo(count)));
    return self();
  }

  public SocialsStage the_actor_received_only_events_containing_$(@Quoted String filter) {
    long otherSocials = testActor
        .getEvents().stream().filter(s -> s.getContent() != null && !s.getContent().contains(filter)).count();
    assertThat(otherSocials, is(equalTo(0L)));
    return self();
  }

  public SocialsStage the_actor_received_only_events_without_$(@Quoted String filter) {
    long otherSocials = testActor
        .getEvents().stream().filter(s -> s.getContent() != null && s.getContent().contains(filter)).count();
    assertThat(otherSocials, is(equalTo(0L)));
    return self();
  }

  public SocialsStage the_actor_received_only_events_of_type_$(SocialType givenSocialType) {
    long otherSocials = testActor
        .getEvents().stream().filter(s -> !givenSocialType.equals(s.getSocialType())).count();
    assertThat(otherSocials, is(equalTo(0L)));
    return self();
  }

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
  final LocalDate date = LocalDate.parse("2017-02-01", ISO_LOCAL_DATE);

  public SocialsStage the_actor_received_only_events_before_given_date() {
    long otherSocials = testActor
        .getEvents().stream().filter(s -> {
      String timestamp = s.getTimestamp();
      if (Strings.isNullOrEmpty(timestamp)) return false;
      LocalDateTime socialDate = LocalDateTime.parse(timestamp, formatter);
      return !socialDate.isBefore(date.atStartOfDay());
    }).count();
    assertThat(otherSocials, is(equalTo(0L)));
    return self();
  }

  public SocialsStage the_actor_received_only_events_after_given_date() {
    long otherSocials = testActor
        .getEvents().stream().filter(s -> {
          String timestamp = s.getTimestamp();
          if (Strings.isNullOrEmpty(timestamp)) return false;
          LocalDateTime socialDate = LocalDateTime.parse(timestamp, formatter);
          return !socialDate.isAfter(date.atStartOfDay());
        }).count();
    assertThat(otherSocials, is(equalTo(0L)));
    return self();
  }
}
