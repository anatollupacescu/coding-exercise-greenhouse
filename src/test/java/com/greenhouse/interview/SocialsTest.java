package com.greenhouse.interview;

import com.greenhouse.interview.api.SocialType;
import com.tngtech.jgiven.junit.SimpleScenarioTest;
import org.junit.Test;

public class SocialsTest extends SimpleScenarioTest<SocialsStage> {

  @Test
  public void actorReceivesAllSocialsIfNoFilterIsSpecified() {
    given()
        .an_actor_listening_for_all_events();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_a_total_of_$_events(898);
  }

  @Test
  public void actorReceivesSocialsByUser() {
    given()
        .an_actor_listening_for_events_from_user_efteling();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_a_total_of_$_events(40);
  }

  @Test
  public void actorReceivesSocialsContainingDisney() {
    given()
        .an_actor_listening_for_events_containing_disney();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_containing_$("disney");
  }

  @Test
  public void actorReceivesAllSocialsNotContainingDisney() {
    given()
        .an_actor_listening_for_events_not_containing_disney();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_without_$("disney");
  }

  @Test
  public void actorReceivesOnlyVideoContent() {
    given()
        .an_actor_listening_for_events_of_type_video();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_of_type_$(SocialType.INSTAGRAM_VIDEO);
  }

  @Test
  public void actorReceivesSocialsBeforeGivenDate() {
    given()
        .an_actor_listening_for_events_before_given_date();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_before_given_date();

  }

  @Test
  public void actorReceivesSocialsAfterGivenDate() {
    given()
        .an_actor_listening_for_events_after_given_date();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_after_given_date();

  }

  @Test
  public void actorReceivesOnlyFacebookSocials() {
    given()
        .an_actor_listening_for_events_of_type_facebook();

    when()
        .the_events_are_sourced();

    then()
        .the_actor_received_only_events_of_type_$(SocialType.FACEBOOK_POST);
  }
}
