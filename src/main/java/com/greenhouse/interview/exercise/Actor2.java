package com.greenhouse.interview.exercise;

import com.greenhouse.interview.api.Actor;

public class Actor2 extends Actor {

  public Actor2(String name) {
    super(name, (s) -> {
      String content = s.getContent();
      return content != null && content.contains("disney");
    });
  }
}
