package com.greenhouse.interview.exercise;

import com.greenhouse.interview.api.Actor;

public class Actor1 extends Actor {

  public Actor1(String name) {
    super(name, (s) -> "efteling".equals(s.getUsername()));
  }
}
