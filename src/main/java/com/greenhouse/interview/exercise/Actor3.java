package com.greenhouse.interview.exercise;

import com.greenhouse.interview.api.Actor;

public class Actor3 extends Actor {

  public Actor3(String name) {
    super(name, (s) -> {
      String content = s.getContent();
      return content != null && !content.contains("disney");
    });
  }
}
