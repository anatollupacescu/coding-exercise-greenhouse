package com.greenhouse.interview.exercise;

import com.greenhouse.interview.api.Actor;
import com.greenhouse.interview.api.SocialType;

public class Actor4 extends Actor {

  public Actor4(String name) {
    super(name, (s) -> SocialType.INSTAGRAM_VIDEO.equals(s.getSocialType()));
  }
}
