package com.greenhouse.interview.exercise;

import com.greenhouse.interview.api.Actor;
import com.greenhouse.interview.api.SocialType;

public class Actor7 extends Actor {

  public Actor7(String name) {
    super(name, (s) -> SocialType.FACEBOOK_POST.equals(s.getSocialType()));
  }
}
