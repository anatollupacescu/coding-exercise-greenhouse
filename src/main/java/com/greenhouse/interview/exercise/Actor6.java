package com.greenhouse.interview.exercise;

import com.google.common.base.Strings;
import com.greenhouse.interview.api.Actor;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Actor6 extends Actor {

  private final static LocalDate date = LocalDate.of(2017, 2, 1);

  public Actor6(String name) {
    super(name, (s) -> {
      String timestamp = s.getTimestamp();
      if (Strings.isNullOrEmpty(timestamp)) {
        return false;
      }
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      LocalDateTime socialDate = LocalDateTime.parse(timestamp, formatter);
      return socialDate.isAfter(date.atStartOfDay());
    });
  }
}
