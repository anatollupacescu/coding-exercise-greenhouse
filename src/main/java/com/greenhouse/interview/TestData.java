package com.greenhouse.interview;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.greenhouse.interview.api.Social;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;
import lombok.NoArgsConstructor;

public enum TestData {

  INSTANCE;

  final List<Social> socials;

  TestData() {
    JsonReader reader;
    List<Social> diskData = Collections.emptyList();
    try {
      reader = new JsonReader(new FileReader("src/test/resources/socials.json"));
      Gson gson = new Gson();
      Socials socialsRoot = gson.fromJson(reader, Socials.class);
      diskData = socialsRoot.socials;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    this.socials = Collections.unmodifiableList(diskData);
  }

  @NoArgsConstructor
  private static class Socials {

    List<Social> socials;
  }
}
