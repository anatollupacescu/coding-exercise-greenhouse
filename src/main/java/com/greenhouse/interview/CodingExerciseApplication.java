package com.greenhouse.interview;

import com.greenhouse.interview.api.Actor;
import com.greenhouse.interview.exercise.Actor1;
import com.greenhouse.interview.exercise.Actor2;
import com.greenhouse.interview.exercise.Actor3;
import com.greenhouse.interview.exercise.Actor4;
import com.greenhouse.interview.exercise.Actor5;
import com.greenhouse.interview.exercise.Actor6;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

@Slf4j
public class CodingExerciseApplication {

  private final SocialsFacade facade = new SocialsFacade(TestData.INSTANCE.socials);

  public static void main(String[] args) {
    val app = new CodingExerciseApplication();
    app.run();
    app.shutDown();
  }

  private void run() {
    log.info("Starting off...");
    for (int i = 0; i < 100; i ++) {
      Actor actor = randomActor(i);
      CountDownLatch latch = new CountDownLatch(1);
      facade.requestAccess(actor, latch);
    }
  }

  private Actor randomActor(int i) {
    int anInt = new Random().nextInt(6) + 1;
    switch (anInt) {
      case 1: return new Actor1("actorOne-" + i);
      case 2: return new Actor2("actorTwo-" + i);
      case 3: return new Actor3("actorTree-" + i);
      case 4: return new Actor4("actorFour-" + i);
      case 5: return new Actor5("actorFive-" + i);
      case 6: return new Actor6("actorSix-" + i);
      default:
        throw new IllegalArgumentException("Ololo " + anInt);
    }
  }

  private void shutDown() {
    facade.shutDown();
    System.exit(1);
  }
}
