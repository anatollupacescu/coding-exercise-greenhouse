package com.greenhouse.interview.api;

import lombok.Value;

@Value
public class Social {

  private SocialType socialType;
  private String socialId;
  private String timestamp;
  private String username;
  private Long userId;
  private String content;
  private Float latitude;
  private Float longitude;
}
