package com.greenhouse.interview.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import lombok.Data;

@Data
public class Actor implements Comparable {

  private final String name;
  private final Predicate<Social> filter;
  private final List<Social> events;

  public Actor(String name, Predicate<Social> p) {
    this.name = name;
    this.filter = p;
    this.events = new ArrayList<>();
  }

  public Integer eventCount() {
    return events.size();
  }

  public void accept(Social social) {
    if (filter.test(social)) {
      events.add(social);
    }
  }

  @Override
  public int compareTo(Object o) {
    if(this.equals(o)) {
      return 0;
    }
    return -1;
  }
}
