package com.greenhouse.interview.api;

public enum SocialType {
  INSTAGRAM_PICTURE,
  INSTAGRAM_VIDEO,
  FACEBOOK_POST
}
