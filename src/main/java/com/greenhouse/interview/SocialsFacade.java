package com.greenhouse.interview;

import com.greenhouse.interview.api.Actor;
import com.greenhouse.interview.api.Social;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

@Slf4j
public class SocialsFacade {

  private final ExecutorService executorService = Executors.newFixedThreadPool(5);
  private final Scheduler scheduler = Schedulers.from(executorService);
  private final Flowable<Social> observable;

  public SocialsFacade(List<Social> socials) {
    observable = Flowable.fromIterable(socials);
  }

  public void requestAccess(Actor actor, CountDownLatch latch) {
    observable
        .observeOn(scheduler)  //the computation (filtering) is going to take place in a thread pool
        .subscribe(new Subscriber<Social>() {

          long start;

          @Override
          public void onSubscribe(Subscription subscription) {
            log.info("Actor '{}' is requesting socials...", actor.getName());
            start = System.nanoTime();
            subscription.request(Integer.MAX_VALUE); //infinite seq
          }

          @Override
          public void onNext(Social social) {
            actor.accept(social);
          }

          @Override
          public void onError(Throwable throwable) {
            log.error("Could not fetch event", throwable);
          }

          @Override
          public void onComplete() {
            latch.countDown();
            val value = System.nanoTime() - start;
            val elapsed = TimeUnit.NANOSECONDS.toMillis(value);
            log.info("Actor {} has received {} records in {} millis", actor.getName(), actor.getEvents().size(), elapsed);
          }
        });
  }

  public void shutDown() {
    executorService.shutdown();
    log.info("Waiting for threads to finish...");
    try {
      executorService.awaitTermination(3, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      log.error("Could not shutdown executor service", e);
    }
    log.info("Exiting...");
    scheduler.shutdown();
  }
}
