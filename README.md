This is a command line application, run it using

    ./mvnw clean compile exec:java
    
It's going to spawn 100 actors (0..99) that will request different socials from an in-memory collection (socials.json)
and then each actor will print to the console how many socials it has fetched (based on its filter).

The concurrency is handle by RxJava2 library and is leveraging a java executor service of fixed size.